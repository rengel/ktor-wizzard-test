package com.example.wizzard

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.util.pipeline.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.html.*
import java.net.URLEncoder
import java.util.*
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.time.Duration.Companion.seconds

class WizzardManager {
    private var runningWizzards = mapOf<String, Wizzard>()

    fun start(wizzard: Wizzard, pipelineContext: PipelineContext<Unit, ApplicationCall>) {
        val id = UUID.randomUUID().toString()
        runningWizzards += id to wizzard
        pipelineContext.apply {
            runBlocking { call.respondRedirect("http://localhost:8080/wizzard/${URLEncoder.encode(id, "utf-8")}") }
        }
    }

    suspend fun next(id: String, pipelineContext: PipelineContext<Unit, ApplicationCall>) = pipelineContext.apply {
        val wizzard = runningWizzards[id]
        if (wizzard == null) {
            call.respond(HttpStatusCode.NotFound, "Not found")
            return@apply
        }

        suspendCoroutine { continuation ->
            synchronized(wizzard) {
                wizzard.onSuspend {
                    continuation.resume(Unit)
                }

                if (!wizzard.isInitialized()) {
                    wizzard.init(pipelineContext)
                } else {
                    wizzard.continuation?.resume(pipelineContext)
                }
            }
        }
    }
}

abstract class Wizzard() {
    var continuation: Continuation<PipelineContext<Unit, ApplicationCall>>? = null

    private var currentPipelineContext: PipelineContext<Unit, ApplicationCall>? = null

    fun isInitialized() = currentPipelineContext != null

    fun init(pipelineContext: PipelineContext<Unit, ApplicationCall>) {
        currentPipelineContext = pipelineContext
        pipelineContext.run { launch { run() } }
    }

    abstract suspend fun run()
    suspend fun waitForCall(sendForm: suspend PipelineContext<Unit, ApplicationCall>.() -> Unit): PipelineContext<Unit, ApplicationCall> {
        currentPipelineContext?.apply { sendForm() }

        return suspendCoroutine { c ->
            this.continuation = c
            onSuspend()
        }.also { currentPipelineContext = it }
    }

    suspend fun endFlow(send: suspend PipelineContext<Unit, ApplicationCall>.() -> Unit) {
        currentPipelineContext?.apply { send() }
        onSuspend()
    }

    private var onSuspend = {}
    fun onSuspend(block: () -> Unit) {
        onSuspend = block
    }
}

data class MyData1(val surname: String, val firstname: String)

data class Message(val message: String)


class MyWizzard : Wizzard() {

    private suspend fun getMyData1(): MyData1 = waitForCall {
        call.respondHtml {
            body {
                form(action = "#", encType = FormEncType.applicationXWwwFormUrlEncoded, method = FormMethod.post) {
                    p {
                        +"Vorname"
                        textInput(name = "firstname")
                    }

                    p {
                        +"Nachname"
                        textInput(name = "surname")
                    }

                    submitInput { value = "Senden" }
                }
            }
        }
    }.run {
        val form = call.receiveParameters()
        MyData1(form["surname"].toString(), form["firstname"].toString())
    }

    private suspend fun getMessage(): Message = waitForCall {
        call.respondHtml {
            body {
                form(action = "#", encType = FormEncType.applicationXWwwFormUrlEncoded, method = FormMethod.post) {
                    p {
                        +"Nachricht"
                        textInput(name = "message")
                    }

                    submitInput { value = "Senden" }
                }
            }
        }
    }.run {
        val form = call.receiveParameters()
        Message(form["message"].toString())
    }

    override suspend fun run() {
        val myData1 = getMyData1()
        val message = getMessage()

        endFlow {
            println("delay")
            delay(5.seconds)
            println("continue")
            call.respondHtml {
                body {
                    p { +"$myData1" }
                    p { +message.message }
                }
            }
        }
    }

}
