package com.example.plugins

import com.example.wizzard.MyWizzard
import com.example.wizzard.WizzardManager
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureRouting() {
    routing {
        get("/") {
            call.respondText("Hello World!")
        }

        route("/wizzard") {
            val wm = WizzardManager()

            get {
                wm.start(MyWizzard(), this)
            }
            get("/{id}") {
                val id = call.parameters["id"]!!
                wm.next(id, this)
            }
            post("/{id}") {
                val id = call.parameters["id"]!!
                wm.next(id, this)
            }
        }
    }
}
